/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_AMBIENTFOREST_SWAMPPROTOTYPING1 = 1171642584U;
        static const AkUniqueID PLAY_AMBIENTSWAMPPROTOTYPING = 1741864169U;
        static const AkUniqueID PLAY_BEETLER = 3151395335U;
        static const AkUniqueID PLAY_DRAGONFLYIDLE = 2418473464U;
        static const AkUniqueID PLAY_FLUTTERFLY = 722785571U;
        static const AkUniqueID PLAY_FLY = 2941622701U;
        static const AkUniqueID PLAY_FOOTSTEPS = 3854155799U;
        static const AkUniqueID PLAY_MEDIUMEINSECTIDLE = 428289258U;
        static const AkUniqueID PLAY_MOTHCLUSTER = 4048717744U;
        static const AkUniqueID PLAY_MOTHNOISES = 3354896931U;
        static const AkUniqueID PLAY_MOTHRAALERT = 2537767465U;
        static const AkUniqueID PLAY_MUSIC = 2932040671U;
        static const AkUniqueID PLAY_NETSWIPE = 488037735U;
        static const AkUniqueID PLAY_POLKAR = 1824308303U;
        static const AkUniqueID PLAY_RIBBIT = 536962038U;
        static const AkUniqueID PLAY_SMALLINSECTIDLE = 3713128733U;
        static const AkUniqueID PLAY_SNAILALERT = 2281274877U;
        static const AkUniqueID PLAY_SNAILMOVE1 = 2892850555U;
        static const AkUniqueID PLAY_UI_BACK = 1386224142U;
        static const AkUniqueID PLAY_UI_FORWARD = 2563718726U;
        static const AkUniqueID STOP_BEETLER = 2722438425U;
        static const AkUniqueID STOP_DRAGONFLYIDLE = 2796527366U;
        static const AkUniqueID STOP_FLUTTERFLY = 4102316125U;
        static const AkUniqueID STOP_FLUTTERFLY_01 = 1094889465U;
        static const AkUniqueID STOP_FLY = 419831083U;
        static const AkUniqueID STOP_MEDIUMEINSECTMOVE = 1238951921U;
        static const AkUniqueID STOP_MOTHCLUSTER = 2648390466U;
        static const AkUniqueID STOP_MOTHNOISES = 1736593997U;
        static const AkUniqueID STOP_POLKAR = 4002194881U;
        static const AkUniqueID STOP_SMALLINSECTIDLE = 2018277687U;
        static const AkUniqueID STOP_SNAILMOVE1 = 3273339457U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSIC
        {
            static const AkUniqueID GROUP = 3991942870U;

            namespace STATE
            {
                static const AkUniqueID HUB = 646625282U;
                static const AkUniqueID MOTH = 3044759701U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID SNAIL = 3154020832U;
                static const AkUniqueID WATER = 2654748154U;
            } // namespace STATE
        } // namespace MUSIC

    } // namespace STATES

    namespace SWITCHES
    {
        namespace STEPS
        {
            static const AkUniqueID GROUP = 1718617278U;

            namespace SWITCH
            {
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID GRAVEL = 2185786256U;
                static const AkUniqueID ROCK = 2144363834U;
                static const AkUniqueID SWAMP = 2907906111U;
            } // namespace SWITCH
        } // namespace STEPS

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID MASTERVOLUME = 2918011349U;
        static const AkUniqueID MUSICVOLUME = 2346531308U;
        static const AkUniqueID SFXVOLUME = 988953028U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN_BANK = 3989171651U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
