using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialScript : MonoBehaviour
{
    [SerializeField]
    private PlayerAudio.Footsteps sound = default;

    public PlayerAudio.Footsteps GetSound()
	{
        return sound;
	}
}
