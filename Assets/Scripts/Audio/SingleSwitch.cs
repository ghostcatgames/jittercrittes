using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSwitch : MonoBehaviour
{
    [SerializeField]
    AK.Wwise.Switch _enterSwitch = default;

    [SerializeField]
    AK.Wwise.Switch _exitSwitch = default;

    //Once the player enters the trigger for this object, it will play a singular sound and then not be triggered again
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == Player.Instance.transform)
        {
            _enterSwitch.SetValue(Player.Instance.transform.gameObject);
        }
    }

	private void OnTriggerExit(Collider other)
	{
        if (other.transform == Player.Instance.transform)
        {
            _exitSwitch.SetValue(Player.Instance.transform.gameObject);
        }
    }
}
