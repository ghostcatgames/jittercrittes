using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySingleAudio : MonoBehaviour
{
    [SerializeField]
    AK.Wwise.Event sound = default;

    [SerializeField]
    bool playMultiple = false;

    //Once the player enters the trigger for this object, it will play a singular sound and then not be triggered again
    private void OnTriggerEnter(Collider other)
    {
        if(other.transform == Player.Instance.transform)
        {
            AkSoundEngine.PostEvent(sound.Name, this.gameObject);

            if (!playMultiple)
            {
                Destroy(this);
            }
        }
    }
}
