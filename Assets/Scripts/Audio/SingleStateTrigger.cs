using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleStateTrigger : MonoBehaviour
{
    [SerializeField]
    AK.Wwise.State state = default;

    [SerializeField]
    bool playMultiple = false;

    [SerializeField]
    private AudioHandler.MusicState s = default;

    //Once the player enters the trigger for this object, it will play a singular sound and then not be triggered again
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == Player.Instance.transform)
        {
            if (AudioHandler.Instance.SetCurrentMusic(s))
            {
                state.SetValue();
                if (!playMultiple)
                {
                    Destroy(this);
                }
            }
        }
    }

	private void OnTriggerExit(Collider other)
	{
        if (other.transform == Player.Instance.transform)
        {
            AudioHandler.Instance.SetGameMusicState();
            AudioHandler.Instance.SetMusicState(AudioHandler.MusicState.GAME);
        }
    }
}
