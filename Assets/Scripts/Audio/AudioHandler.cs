using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Audios
{
    public string audioName = "";
    public AK.Wwise.Event sound = default;
}

public class AudioHandler : MonoBehaviour
{
    public static AudioHandler Instance = default;

    [SerializeField]
    private List<Audios> allAudioClips = new List<Audios>();

    [SerializeField]
    private Slider masterSlider = default;

    [SerializeField]
    private Slider effectsSlider = default;

    [SerializeField]
    private Slider musicSlider = default;

    [SerializeField]
    private AK.Wwise.State mainMenuState = default;

    [SerializeField]
    private AK.Wwise.State mainGameState = default;

    [SerializeField]
    private AK.Wwise.Bank bank = default;

    [SerializeField]
    private AK.Wwise.Event normalClick = default;

    [SerializeField]
    private AK.Wwise.Event backClick = default;

    public enum MusicState { GAME, MARSH, SNAIL, MOTH}
    private MusicState currentMusicState = default;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        mainMenuState.SetValue();

        AkSoundEngine.LoadBank(bank.Id);
    }

    public void PlaySound(string s)
	{
        foreach(Audios a in allAudioClips)
		{
            if(a.audioName == s)
			{
                AkSoundEngine.PostEvent(a.sound.Name, this.gameObject);
                break;
            }
        }
	}

	private void FixedUpdate()
	{
        SetSliderValue();
	}

	public void SetSliderValue()
	{
        AkSoundEngine.SetRTPCValue("Effects", effectsSlider.value);
        AkSoundEngine.SetRTPCValue("MusicVolume", musicSlider.value);
        AkSoundEngine.SetRTPCValue("MasterVolume", masterSlider.value);
    }

    public void PlayClick()
	{
        AkSoundEngine.PostEvent(normalClick.Name, this.gameObject);
    }

    public void PlayBack()
	{
        AkSoundEngine.PostEvent(backClick.Name, this.gameObject);
    }

    public void SetGameMusicState()
	{
        mainGameState.SetValue();
	}

    public bool SetCurrentMusic(MusicState s)
	{
        if(s != currentMusicState)
		{
            currentMusicState = s;
            return true;
		}

        return false;
	}

    public void SetMusicState(MusicState s)
	{
        currentMusicState = s;
	}

}
