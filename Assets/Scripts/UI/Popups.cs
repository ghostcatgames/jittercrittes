using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Popups : MonoBehaviour
{
    public static Popups Instance = default;

    [SerializeField]
    private GameObject popupPrefab = default;

    [SerializeField]
    private Transform popupParent = default;

    [SerializeField]
    private float popupTime = 1;

    IEnumerator PopupDelete(GameObject g)
	{
        float timer = popupTime;

        while(timer > 0)
		{
            timer -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
		}

        JournalHandler.Instance.HideIcons();
        Destroy(g);
	}

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public void NewPopup(Texture s, string t)
	{
        GameObject popup = Instantiate(popupPrefab);
        popup.GetComponentInChildren<RawImage>().texture= s;
        popup.GetComponentInChildren<TextMeshProUGUI>().text = t;
        popup.transform.SetParent(popupParent);

        StartCoroutine(PopupDelete(popup));
	}
}
