using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class MenuData
{
    public enum MenuName { MAINMENU = 1, JOURNAL = 2, GAMEUI = 3, SETTINGS = 4, CREDITS = 5, PAUSE = 6}

    public MenuName menuName = default;
    public GameObject menuObject = default;
}

public class UIHandler : MonoBehaviour
{
    public static UIHandler Instance = default;

    [SerializeField]
    private List<MenuData> allMenus = new List<MenuData>();

    MenuData.MenuName currentMenu = default;

    [SerializeField]
    private bool inMainMenu = false;

    private bool inPauseMenu = false;

    [SerializeField]
    private Animator anim = default;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        SwitchMenu(MenuData.MenuName.MAINMENU);

        SetInMainMenu(inMainMenu);
    }

    public void SwitchMenu(MenuData.MenuName name)
	{
        if(name == MenuData.MenuName.GAMEUI)
		{
            if (inMainMenu)
            {
                name = MenuData.MenuName.MAINMENU;
                currentMenu = name;
            }
            else if(inPauseMenu)
			{
                name = MenuData.MenuName.PAUSE;
                currentMenu = name;
            }
		}


        foreach(MenuData m in allMenus)
		{
            if(m.menuName == name)
			{
                m.menuObject.SetActive(true);
                currentMenu = name;
			}
			else
			{
                m.menuObject.SetActive(false);
			}
		}

        if(currentMenu == MenuData.MenuName.GAMEUI)
		{
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
		}
		else
		{
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
	}

    public MenuData.MenuName GetCurrentMenu()
	{
        return currentMenu;
	}

    public bool GetInMenu()
	{
        if(currentMenu == MenuData.MenuName.GAMEUI && !inMainMenu && !inPauseMenu)
		{
            return false;
		}

        return true;
	}

    public void SetInMainMenu(bool set)
	{
        inMainMenu = set;

        if (!set)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void SetInPauseMenu(bool set)
	{
        inPauseMenu = set;

        if (!set)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void SwitchToGameScene(string scene)
	{
        StartCoroutine(LoadNewScene(scene, MenuData.MenuName.GAMEUI));
    }

    public void SwitchToMainMenuScene(string scene)
    {
        StartCoroutine(LoadNewScene(scene, MenuData.MenuName.MAINMENU));
    }

    IEnumerator LoadNewScene(string scene, MenuData.MenuName m)
	{
        anim.SetTrigger("FadeOut");

        yield return new WaitForSecondsRealtime(1);

        SceneManager.LoadScene(scene);

        while(!SceneManager.GetSceneByName(scene).isLoaded)
		{
            yield return new WaitForFixedUpdate();
		}

        anim.SetTrigger("FadeIn");

        yield return new WaitForSecondsRealtime(0.3f);

        SwitchMenu(m);

    }
}
