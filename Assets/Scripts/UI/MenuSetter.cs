using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSetter : MonoBehaviour
{
    [SerializeField]
    private MenuData.MenuName name = default;

	private void OnEnable()
	{
		if (UIHandler.Instance != null)
		{
			if (name == MenuData.MenuName.PAUSE)
			{
				UIHandler.Instance.SetInPauseMenu(true);
			}
			else
			{
				UIHandler.Instance.SetInPauseMenu(false);
			}
		}
	}

}
