using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButton : MonoBehaviour
{
    [SerializeField]
    private MenuData.MenuName menuSwitch = default;

    // Start is called before the first frame update
    void Start()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(ChangeMenu);
    }

    void ChangeMenu()
	{
        UIHandler.Instance.SwitchMenu(menuSwitch);
	}
}
