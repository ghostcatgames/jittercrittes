using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SubMenuData
{
    public string MenuName = default;
    public GameObject menuObject = default;
}

public class SubUIHandler : MonoBehaviour
{
    [SerializeField]
    private List<SubMenuData> allMenus = new List<SubMenuData>();

    private string currentMenu = default;

	private void OnEnable()
	{
        SwitchMenu(allMenus[0].MenuName);
	}

	public void SwitchMenu(string name)
    {
        foreach (SubMenuData m in allMenus)
        {
            if (m.MenuName == name)
            {
                m.menuObject.SetActive(true);
                currentMenu = name;
            }
            else
            {
                m.menuObject.SetActive(false);
            }
        }

        currentMenu = name;
    }

}
