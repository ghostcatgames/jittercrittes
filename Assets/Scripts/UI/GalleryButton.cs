using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GalleryButton : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI nameMesh = default;

	[SerializeField]
	private RawImage bugSprite = default;

	private BugData data = default;

	private void Start()
	{
		Button btn = GetComponent<Button>();
		btn.onClick.AddListener(UpdateJournal);
	}
	public void AssignData( BugData d)
	{
		nameMesh.text = d.bugJournal.BugName;
		bugSprite.texture = JournalHandler.Instance.GetBugTex(d.bugJournal.iconID);

		data = d;
	}

	void UpdateJournal()
	{
		JournalHandler.Instance.FillJournalPage(data);
	}

	public BugData GetBugData()
	{
		return data;
	}
}
