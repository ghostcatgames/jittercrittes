using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugEmoticons : MonoBehaviour
{
    public static BugEmoticons Instance = default;

    [SerializeField]
    private Sprite startled = default;

    [SerializeField]
    private Sprite docile = default;

    [SerializeField]
    private Sprite idle = default;

    public enum Emoticons { STARTLED, DOCILE, IDLE}

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public Sprite GetSprite(Emoticons e)
	{
        switch(e)
		{
            case Emoticons.STARTLED:
                return startled;
            case Emoticons.DOCILE:
                return docile;
            case Emoticons.IDLE:
                return idle;
		}

        return null;
	}
}
