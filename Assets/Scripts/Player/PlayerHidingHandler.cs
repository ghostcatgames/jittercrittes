using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHidingHandler : MonoBehaviour
{
    public static PlayerHidingHandler Instance = default;

    private BossBug boss = default;

    private RaycastHit hidingHit = default;

    [SerializeField]
    private float hidingDistCheck = 0;

    private Vector3 directionToBoss = default;

    [SerializeField]
    private LayerMask hidingSpotLayer = default;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(boss != null)
		{
            directionToBoss = boss.GetBoss().transform.position - transform.position;

            //Hiding raycast
            Physics.Raycast(transform.position, directionToBoss, out hidingHit, hidingDistCheck, hidingSpotLayer);

            if(hidingHit.transform != null && !boss.GetCanSeePlayer())
			{
                Debug.Log("IS HIDING");
                Player.Instance.SetPlayerHiding(true);
			}
			else if(boss.GetCanSeePlayer())
			{
                Debug.Log("NOT HIDING");
                Player.Instance.SetPlayerHiding(false);
			}
        }
    }

    public void SetCurrentBoss(BossBug b)
	{
        boss = b;
	}

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + directionToBoss / hidingDistCheck);
    }
}
