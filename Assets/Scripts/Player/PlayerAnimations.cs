using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    public static PlayerAnimations Instance = default;

    [SerializeField]
    private Animator anim = default;

    
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public void PlayNetAnimation()
	{
        anim.SetTrigger("Net");
	}
}
