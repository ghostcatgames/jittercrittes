using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public static Player Instance = default;

    [SerializeField]
    private CharacterController controller = default;

    [SerializeField]
    [Range(1, 10)]
    private float speed = 0f;

    [SerializeField]
    private float minCamAngle = -90;

    [SerializeField]
    private float maxCameraAngle = 60;

    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float mouseSensitivity = 0.3f;

    [SerializeField]
    private float interactionRange = 1;

    [SerializeField]
    private float headJogSpeed = 0;

    [SerializeField]
    [Range(0.01f, 1)]
    private float jogHeadBobRange = 0;

    [SerializeField]
    private float headIdleSpeed = 0;

    [SerializeField]
    [Range(0.01f, 1)]
    private float idleHeadBobRange = 0;

    [SerializeField]
    private float jumpHeight = 0;
    private bool isGrounded = false;
    private Vector3 playerVelocity;

    private float currentHeadSpeed = 0;
    private Vector3 higherHeadRange = default;
    private Vector3 lowerHeadRange = default;

    [SerializeField]
    private bool headBobs = false;

    Vector3 dir = default;

    private float gravity = 0;
    private float xRotation = 0;
    private float yRotation = 0;

    private Vector2 mouseDir = default;

    private Camera cam = default;
    private Vector3 camStartingPos = default;

    private RaycastHit hit = default;

    private bool isHiding = default;

    [SerializeField]
    private Vector3 hidingOffset = default;

    private Vector3 topRange = default;
    private Vector3 bottomRange = default;

    private bool stopInput = false;

    [SerializeField]
    private LayerMask interactableLayer = default;

    private bool justThrown = false;

    IEnumerator HeadBob()
	{
        bool bobDown = false;

        while (headBobs)
		{
            //If hiding then lower the camera range to emulate hiding

            if (!bobDown)
            {
                cam.transform.localPosition = Vector3.MoveTowards(cam.transform.localPosition, topRange, Time.deltaTime * currentHeadSpeed);
            }
			else
			{
                cam.transform.localPosition = Vector3.MoveTowards(cam.transform.localPosition, bottomRange, Time.deltaTime * currentHeadSpeed);
            }

            if(cam.transform.localPosition.y >= topRange.y)
			{
                bobDown = true;
			}
            else if(cam.transform.localPosition.y <= bottomRange.y)
			{
                bobDown = false;

                if (dir != Vector3.zero && isGrounded)
                {
                    PlayerAudio.Instance.PlayFootstep();
                }
			}

            yield return new WaitForFixedUpdate();
		}
	}

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        cam = Camera.main;
        camStartingPos = cam.transform.localPosition;

        //Starting Values
        currentHeadSpeed = headIdleSpeed;

        higherHeadRange = camStartingPos + new Vector3(0, idleHeadBobRange, 0);
        lowerHeadRange = camStartingPos - new Vector3(0, idleHeadBobRange, 0);

        topRange = higherHeadRange;
        bottomRange = lowerHeadRange;

        StartCoroutine(HeadBob());

    }

    // Update is called once per frame
    void Update()
    {
        if (UIHandler.Instance != null)
        {
            if (!UIHandler.Instance.GetInMenu() && !stopInput)
            {
                if (dir != Vector3.zero)
                {
                    Vector3 d = (transform.right * dir.x * Time.deltaTime * speed) + (transform.forward * dir.y * Time.deltaTime * speed);
                    controller.Move(d);
                }

                if (!isGrounded)
                {
                    playerVelocity.y += gravity * Time.deltaTime * jumpHeight;
                    controller.Move(playerVelocity * Time.deltaTime);
                }

                Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, interactionRange, interactableLayer);
            }
        }
    }

    private void FixedUpdate()
    {
        gravity = Physics.gravity.y / 2 * Time.deltaTime;

        if (!isGrounded && !UIHandler.Instance.GetInMenu() && !stopInput)
        {
            playerVelocity.y += gravity * Time.deltaTime * jumpHeight;
            controller.Move(playerVelocity * Time.deltaTime);
        }
    }

    private void LateUpdate()
    {
        if (UIHandler.Instance != null)
        {
            if (!UIHandler.Instance.GetInMenu())
            {
                yRotation += mouseDir.x * mouseSensitivity;
                xRotation -= mouseDir.y * mouseSensitivity;
                xRotation = Mathf.Clamp(xRotation, minCamAngle, maxCameraAngle);

                cam.transform.localRotation = Quaternion.Euler(new Vector3(xRotation, 0.0f, 0.0f));
                transform.eulerAngles = new Vector3(0, yRotation, 0);
            }
        }
    }

    public void OnMove(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            dir = ctx.ReadValue<Vector2>();
            currentHeadSpeed = headJogSpeed;
            higherHeadRange = camStartingPos + new Vector3(0, jogHeadBobRange, 0);
            lowerHeadRange = camStartingPos - new Vector3(0, jogHeadBobRange, 0);
        }
        else
        {
            dir = Vector2.zero;
            currentHeadSpeed = headIdleSpeed;
            higherHeadRange = camStartingPos + new Vector3(0, idleHeadBobRange, 0);
            lowerHeadRange = camStartingPos - new Vector3(0, idleHeadBobRange, 0);
        }
    }

    public void OnLookAt(InputAction.CallbackContext ctx)
    {
        mouseDir = ctx.ReadValue<Vector2>();
    }

    public void OnInteract(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            if (UIHandler.Instance != null)
            {
                if (!UIHandler.Instance.GetInMenu() && !PlayerUI.Instance.CatchCooldown())
                {
                    if (hit.transform != null)
                    {
                        Bug bugHit = null;
                        Interactable interactable = null;

                        hit.transform.TryGetComponent<Bug>(out bugHit);

                        if(bugHit == null)
						{
                            bugHit = hit.transform.GetComponentInParent<Bug>();
                        }

                        hit.transform.TryGetComponent<Interactable>(out interactable);

                        if (bugHit != null && !PlayerUI.Instance.CatchCooldown())
                        {
                            if (bugHit.GetCanCapture())
                            {
                                bugHit.CatchBug();
                                Debug.Log("CATCH BUG");
                            }
                        }
                        else if (interactable != null && !PlayerUI.Instance.TapCooldown())
                        {
                            interactable.Interact();
                            StartCoroutine(PlayerUI.Instance.Tap());
                        }
                    }
                    else
                    {
                        Debug.Log("HIT IS NULL");
                    }

                    PlayerAnimations.Instance.PlayNetAnimation();
                    StartCoroutine(PlayerUI.Instance.Catch());
                    AudioHandler.Instance.PlaySound("NetSwing");
                }
            }
        }
    }

    public void OnJump(InputAction.CallbackContext ctx)
	{
        if(ctx.performed && isGrounded)
		{
            isGrounded = false;
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravity);
        }
	}

    public void OnOpenJournal(InputAction.CallbackContext ctx)
	{
        if(ctx.performed && UIHandler.Instance.GetCurrentMenu() == MenuData.MenuName.GAMEUI)
		{
            if (UIHandler.Instance != null)
            {
                UIHandler.Instance.SwitchMenu(MenuData.MenuName.JOURNAL);
            }
		}
	}

    public void OnOpenSettings(InputAction.CallbackContext ctx)
	{
        if (ctx.performed && UIHandler.Instance.GetCurrentMenu() == MenuData.MenuName.GAMEUI)
        {
            if (UIHandler.Instance != null)
            {
                UIHandler.Instance.SwitchMenu(MenuData.MenuName.SETTINGS);
            }
        }
    }

    public void OnOpenPause(InputAction.CallbackContext ctx)
    {
        if (ctx.performed && UIHandler.Instance.GetCurrentMenu() == MenuData.MenuName.GAMEUI)
        {
            if (UIHandler.Instance != null)
            {
                UIHandler.Instance.SwitchMenu(MenuData.MenuName.PAUSE);
            }
        }
    }

    public void SetPlayerHiding(bool set)
	{
        if (set != isHiding)
        {
            isHiding = set;

            if (isHiding)
            {
                topRange = higherHeadRange - hidingOffset;
                bottomRange = lowerHeadRange - hidingOffset;
            }
            else
            {
                topRange = higherHeadRange;
                bottomRange = lowerHeadRange;
            }
        }

    }

    public void ThrowPlayer(Vector3 f)
	{
        justThrown = true;
        playerVelocity = new Vector3(Mathf.Sqrt(jumpHeight * -3.0f * gravity) / 2, Mathf.Sqrt(jumpHeight * -3.0f * gravity) / 2, Mathf.Sqrt(jumpHeight * -3.0f * gravity) / 2) + f;
    }

    public void StopPlayerInput(bool set)
	{
        stopInput = set;
    }

    public void EnableController(bool set)
    {
        controller.enabled = set;
    }

	private void OnTriggerEnter(Collider other)
	{
        if (other.transform.tag == "Ground")
        {
            isGrounded = true;
            playerVelocity.y = 0;

            if(justThrown)
			{
                playerVelocity = Vector3.zero;
                justThrown = false;
			}
        }
    }

	private void OnTriggerStay(Collider other)
	{
        if (other.transform.tag == "Ground")
        {
            isGrounded = true;
            playerVelocity.y = 0;
        }
    }

	private void OnTriggerExit(Collider other)
	{
        if (other.transform.tag == "Ground")
        {
            isGrounded = false;
        }
    }
}
