using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public static PlayerUI Instance = default;

    [SerializeField]
    private float catchCoolDown = default;

    [SerializeField]
    private Image catchPic = default;

    [SerializeField]
    private float tapCooldown = default;

    [SerializeField]
    private Image tapPic = default;

    private bool onCatchCooldown = false;
    private bool onTapCooldown = false;

    [SerializeField]
    private Image detectionUI = default;

    [SerializeField]
    private Sprite seenUI = default;

    [SerializeField]
    private Sprite hidingUI = default;


    IEnumerator Cooldown(float time, Image pic)
	{
        float remainingTime = time;
        float increment = 0;

        pic.fillAmount = increment;

        while (remainingTime > 0)
        {
            remainingTime -= Time.deltaTime;
            increment = remainingTime / time;

            pic.fillAmount = increment;

            yield return new WaitForFixedUpdate();
        }

        pic.fillAmount = 1;
    }

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        detectionUI.gameObject.SetActive(false);
    }

    public IEnumerator Catch()
	{
        onCatchCooldown = true;
        yield return StartCoroutine(Cooldown(catchCoolDown, catchPic));
        onCatchCooldown = false;
    }

    public bool CatchCooldown()
	{
        return onCatchCooldown;
	}

    public IEnumerator Tap()
    {
        onTapCooldown = true;
        yield return StartCoroutine(Cooldown(tapCooldown, tapPic));
        onTapCooldown = false;
    }

    public bool TapCooldown()
    {
        return onTapCooldown;
    }
}
