using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    public static PlayerAudio Instance = default;

    [SerializeField]
    private AK.Wwise.Event footsteps = default;

    public enum Footsteps { GRASS, GRAVEL, SWAMP, WOOD}

    RaycastHit hit = default;

    private MaterialScript area = default;

    [SerializeField]
    private LayerMask playerLayer = default;

    private Footsteps currentFootsteps = default;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

	private void FixedUpdate()
	{
        Physics.Raycast(transform.position, Vector3.down, out hit, 1000, ~playerLayer);

        if (hit.transform != null)
        {
            hit.transform.TryGetComponent<MaterialScript>(out area);

            if(area != null)
			{
                SwitchFootsteps(area.GetSound());
			}
			else
			{
                area = hit.transform.GetComponentInChildren<MaterialScript>();

                if (area != null)
                {
                    SwitchFootsteps(area.GetSound());
                }
            }
        }
    }

    public void SwitchFootsteps(Footsteps f)
	{
       
            switch (f)
            {
                case Footsteps.GRASS:
                    AkSoundEngine.SetSwitch("Material", "Grass", gameObject);
                    break;
                case Footsteps.GRAVEL:
                    AkSoundEngine.SetSwitch("Material", "Gravel", gameObject);
                    break;
                case Footsteps.SWAMP:
                    AkSoundEngine.SetSwitch("Material", "Swamp", gameObject);
                    break;
                case Footsteps.WOOD:
                    AkSoundEngine.SetSwitch("Material", "Wood", gameObject);
                    break;
            }

            currentFootsteps = f;
        
	}

    public void PlayFootstep()
	{
        AkSoundEngine.PostEvent(footsteps.Name, this.gameObject);
    }
}
