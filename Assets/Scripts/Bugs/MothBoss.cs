using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MothBoss : BossBug
{
    IEnumerator CheckForPlayer()
    {
        GetComponent<Bug>().UpdateCurrentState(Bug.BehaviorState.ALERT);

        isChecking = true;
        canCapture = false;

        movement.SetMovement(false);

        float timer = searchingTime;

        while (timer > 0)
        {
            timer -= Time.deltaTime;

            Physics.Raycast(bug.transform.position, directionToPlayer, out sightHit, lineOfSightRange);

            direction = Player.Instance.transform.position - bug.transform.position;

            bug.transform.rotation = Quaternion.Lerp(bug.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * movement.GetAgent().angularSpeed / 100);

            if (sightHit.transform == Player.Instance.transform && (cone <= coneAngleHalf && cone >= -coneAngleHalf))
            {
                canSeePlayer = true;
            }
            else
            {
                canSeePlayer = false;
            }

            distance = Vector3.Distance(bug.transform.position, Player.Instance.transform.position);

            if(distance < 4)
			{
                timer = 0;
                canSeePlayer = true;

                Debug.Log("GOT TOO CLOSE RUN!");
			}

            yield return new WaitForFixedUpdate();
        }

        if (canSeePlayer)
        {
            Debug.Log("RUN BITCH RUN");
            particles.Play();
            FadeMoth(false);
            yield return StartCoroutine(movement.RunAwayFromPlayerFurthest(null));
            FadeMoth(true);
            particles.Stop();
        }

        isChecking = false;
        movement.SetMovement(true);
        canSeePlayer = false;

        canCapture = true;

        Debug.Log("STOPPED LOOKING");
    }

    private Vector3 direction = default;

    [SerializeField]
    List<Renderer> allRenderers = new List<Renderer>();

    List<Material> allMaterials = new List<Material>();

    [SerializeField]
    private float fadeTime = 0;

    [SerializeField]
    private ParticleSystem particles = default;

    private float distance = 0;

    [SerializeField]
    private GameObject mothBody = default;

    // Start is called before the first frame update
    void Start()
    {
        canCapture = true;

        FadeMoth(true);

        particles.Stop();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (playerInArea)
        {
            directionToPlayer = Player.Instance.transform.position - bug.transform.position;

            cone = Vector3.Angle(bug.transform.forward, directionToPlayer);

            if (!isChecking)
            {
                Physics.Raycast(bug.transform.position, directionToPlayer, out sightHit, lineOfSightRange);

                if (sightHit.transform == Player.Instance.transform && (cone <= coneAngleHalf && cone >= -coneAngleHalf))
                {
                    canSeePlayer = true;
                    Debug.Log("FOUND PLAYER");
                    StartCoroutine(CheckForPlayer());
                }
                else
                {
                    canSeePlayer = false;
                }
            }
        }
    }

    void GatherAllMaterials()
	{
        GameObject mothObj = bug.transform.GetChild(0).GetChild(0).gameObject;

        Debug.Log(mothObj.name);

        foreach(Transform t in mothObj.transform)
		{
            foreach(Material m in t.GetComponent<MeshRenderer>().materials)
			{
                allMaterials.Add(m);
			}
		}
	}

    public void FadeMoth(bool fadeIn)
	{
        if (fadeIn)
        {
            StartCoroutine(FadeMothMaterials(1));
        }
        else
		{
            StartCoroutine(FadeMothMaterials(0));
        }
	}

    IEnumerator FadeMothMaterials(int target)
    {
        float timer = fadeTime;
        float dir = 0;
        float current = 0;

        if(target == 1)
		{
            dir = 0.1f;
            current = 0;
		}
		else
		{
            dir = -0.1f;
            current = 1;
		}


        while (timer > 0)
        {
            timer -= Time.deltaTime;


            if(timer <= fadeTime / 2)
			{
                if (target == 1)
                {
                    mothBody.SetActive(true);
                }
                else
                {
                    mothBody.SetActive(false);
                }
            }
            foreach (Renderer r in allRenderers)
            {
                foreach (Material m in r.sharedMaterials)
                {
                    current += dir * Time.deltaTime;
                    current = Mathf.Clamp(current, -0.01f, 1f);
                    m.SetFloat("Vector1_677dcff79b3b43dca1d43a7e095c664d", current);
                }
            }

            yield return new WaitForFixedUpdate();
        }

        GetComponent<Bug>().UpdateCurrentState(Bug.BehaviorState.IDLE);
    }

}
