using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBug : Bug
{
    [SerializeField]
    protected GameObject bug = default;

    [SerializeField]
    protected float lineOfSightRange = 0;

    [SerializeField]
    protected float searchingTime = 0;

    [SerializeField]
    protected float coneAngleHalf = 0;

    protected RaycastHit sightHit = default;

    protected Vector3 directionToPlayer = default;

    [SerializeField]
    protected bool playerInArea = false;

    protected bool isChecking = false;

    protected float cone = 0;

    [SerializeField]
    protected BugMovement movement = default;

    protected bool canSeePlayer = false;
    
    [SerializeField]
    private BossArea area = default;

    // Start is called before the first frame update
    void Start()
    {
        PlayerHidingHandler.Instance.SetCurrentBoss(this);

        UpdateCurrentState(BehaviorState.IDLE);
    }

	public override void CatchBug()
	{
		base.CatchBug();
        area.BossCaught();
	}

	public GameObject GetBoss()
	{
        return bug;
	}

    public void SetInArea(bool set)
	{
        playerInArea = set;
	}

    public bool GetCanSeePlayer()
	{
        return canSeePlayer;
	}

}
