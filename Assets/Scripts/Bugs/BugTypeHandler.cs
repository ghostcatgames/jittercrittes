using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BugData
{
    public JournalEntry bugJournal = default;
    public int totalCaught = 0;
}

public class BugTypeHandler : MonoBehaviour
{
	public static BugTypeHandler Instance = default;

	[SerializeField]
	private List<BugData> bugsCaught = new List<BugData>();

	private void Start()
	{
		Instance = this;
	}

	private void OnEnable()
	{
		Instance = this;
	}

	public void CaughtBug(JournalEntry bug)
	{
		int check = 0;

		if (bugsCaught.Count > 0)
		{
			foreach (BugData d in bugsCaught)
			{
				if (d.bugJournal == bug)
				{
					d.totalCaught++;
					check++;
					Debug.Log("EXISTING BUG CAUGHT!!");
					break;
				}
			}
		}

		if (check == 0)
		{
			BugData newBug = new BugData();
			newBug.bugJournal = bug;
			newBug.totalCaught = 1;

			bugsCaught.Add(newBug);

			Debug.Log("NEW BUG ADDED!!");


			JournalHandler.Instance.ShowSpecificIcon(newBug.bugJournal.iconID);
			Popups.Instance.NewPopup(JournalHandler.Instance.GetBugTex(newBug.bugJournal.iconID), newBug.bugJournal.BugName);
		}

		Debug.Log("YOU JUST CAUGHT A " + bug.BugName);

		bugsCaught.Sort((a, b) => a.bugJournal.BugName.CompareTo(b.bugJournal.BugName));
	}

	public int GetBugCaughtCount()
	{
		return bugsCaught.Count;
	}

	public BugData GetBugData(int i)
	{
		return bugsCaught[i];
	}

	public List<BugData> GetBugsCaught()
	{
		return bugsCaught;
	}
}

