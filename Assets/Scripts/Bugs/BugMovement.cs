using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEditor;

public class BugMovement : MonoBehaviour
{
    protected NavMeshAgent agent = default;

    [SerializeField]
    private bool flying = default;

    [SerializeField]
    private int lowestWaitTime = 0;

    [SerializeField]
    private int highestWaitTime = 0;

    [SerializeField]
    private List<Transform> patrolPoints = new List<Transform>();

    [SerializeField]
    private bool randomPatrols = false;

    private bool isPatrolling = false;

    private int currentPatrolPoint = 0;

    private float speed = 0;

    [SerializeField]
    protected bool moveOnStart = false;

    [SerializeField]
    private float speedMultiplier = 1;

    [SerializeField]
    private Bug bug = default;

    [SerializeField]
    private GameObject distanceChecker = default;

    public IEnumerator PatrolPoints()
    {
        if (bug != null)
        {
            bug.UpdateCurrentState(Bug.BehaviorState.IDLE);
        }

        float dist = Vector3.Distance(bug.transform.position, patrolPoints[currentPatrolPoint].position);

        float breakoutTime = 5;

        while (isPatrolling)
        {
            if (agent != null)
            {
                if (distanceChecker != null)
                {
                    dist = Vector3.Distance(distanceChecker.transform.position, patrolPoints[currentPatrolPoint].position);
                }
				else
				{
                    dist = Vector3.Distance(bug.transform.position, patrolPoints[currentPatrolPoint].position);
                }

                agent.destination = patrolPoints[currentPatrolPoint].position;

                breakoutTime -= Time.deltaTime;

                if (dist < 2f || breakoutTime < 0)
                {
                    if (randomPatrols)
                    {
                        currentPatrolPoint = Random.Range(0, patrolPoints.Count);
                    }
                    else
                    {
                        currentPatrolPoint++;

                        if (currentPatrolPoint > patrolPoints.Count - 1)
                        {
                            currentPatrolPoint = 0;
                        }
                    }

                    breakoutTime = 5;

                    //Wait a certain amount of time before continuing
                    yield return new WaitForSeconds(Random.Range(lowestWaitTime, highestWaitTime));

                }
            }

            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator PatrolFlyingPoints()
    {
        bug.UpdateCurrentState(Bug.BehaviorState.IDLE);

        GameObject bugObj = agent.gameObject;

        float dist = Vector3.Distance(bug.transform.position, patrolPoints[currentPatrolPoint].position);

        Vector3 direction = default;

        float breakoutTime = 5;

        while (isPatrolling)
        {
           

            if (distanceChecker != null)
            {
                dist = Vector3.Distance(distanceChecker.transform.position, patrolPoints[currentPatrolPoint].position);
            }
            else
            {
                dist = Vector3.Distance(bug.transform.position, patrolPoints[currentPatrolPoint].position);
            }

            bugObj.transform.position = Vector3.MoveTowards(bugObj.transform.position, patrolPoints[currentPatrolPoint].position, Time.deltaTime * agent.speed);

            direction = patrolPoints[currentPatrolPoint].position - bugObj.transform.position;

            bugObj.transform.rotation = Quaternion.Lerp(bugObj.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * agent.angularSpeed);

            breakoutTime -= Time.deltaTime;

            if (dist < 2f || breakoutTime < 0)
            {
                if (randomPatrols)
                {
                    currentPatrolPoint = Random.Range(0, patrolPoints.Count);
                }
                else
                {
                    currentPatrolPoint++;

                    if (currentPatrolPoint > patrolPoints.Count - 1)
                    {
                        currentPatrolPoint = 0;
                    }
                }

                breakoutTime = 5;

                //Wait a certain amount of time before continuing
                yield return new WaitForSeconds(Random.Range(lowestWaitTime, highestWaitTime));

            }
            yield return new WaitForFixedUpdate();
        }
    }


    public IEnumerator RunAwayFromPlayerFurthest(SnailBoss s)
	{
        Debug.Log("RUN AWAY FROM PLAYER");

        StopAllCoroutines();

        bug.UpdateCurrentState(Bug.BehaviorState.ALERT);

        if (s != null)
        {
            s.TossPlayer();
        }

        bool isRunning = false;

        float furthest = default;

        Transform point = default;

        float breakoutTime = 5;

        for (int i = 0; i < patrolPoints.Count; i++)
		{
            if(i == 0)
			{
                furthest = Vector3.Distance(patrolPoints[0].position, Player.Instance.transform.position);
                point = patrolPoints[0];
                currentPatrolPoint = 0;
            }
			else
			{
                float distance = Vector3.Distance(patrolPoints[i].position, Player.Instance.transform.position);

                if(distance > furthest)
				{
                    furthest = distance;
                    point = patrolPoints[i];
                    currentPatrolPoint = i;
				}
			}
		}

        float dist = 0;

        if (bug != null)
        {
            dist = Vector3.Distance(bug.transform.position, point.position);
        }

        isRunning = true;

        SetMovement(true);
        SetSpeed(true);

        while (isRunning)
		{

            breakoutTime -= Time.deltaTime;

            if (distanceChecker != null)
            {
                dist = Vector3.Distance(distanceChecker.transform.position, point.position);
            }
            else
            {
                dist = Vector3.Distance(bug.transform.position, point.position);
            }
            if (flying)
			{
                bug.transform.position = Vector3.MoveTowards(bug.transform.position, point.position, Time.deltaTime * agent.speed);
            }
			else
			{
                agent.destination = point.position;
            }

            if(dist < 0.5f || breakoutTime < 0)
			{
                isRunning = false;
                SetMovement(true);
                breakoutTime = 5;
			}

            yield return new WaitForFixedUpdate();
		}

        isPatrolling = true;

        SetSpeed(false);

        if (!flying)
        {
            StartCoroutine(PatrolPoints());
        }
		else
		{
            StartCoroutine(PatrolFlyingPoints());
		}
	}

    public IEnumerator MoveTowardsGoal(SnailBoss s, Vector3 pos)
	{
        bug.UpdateCurrentState(Bug.BehaviorState.ALERT);

        SetMovement(true);
        SetSpeed(true);

        s.SetSwerving(true);

        float dist = Vector3.Distance(bug.transform.position, pos);

        float breakoutTime = 5;

        while (s.GetIfSwerving())
		{
            breakoutTime -= Time.deltaTime;

            if (distanceChecker != null)
            {
                dist = Vector3.Distance(distanceChecker.transform.position, pos);
            }
            else
            {
                dist = Vector3.Distance(bug.transform.position, pos);
            }
            if (flying)
            {
                bug.transform.position = Vector3.MoveTowards(bug.transform.position, pos, Time.deltaTime * agent.speed);
            }
            else
            {
                agent.destination = pos;
            }

            if(dist < 2 || breakoutTime < 0)
			{
                s.SetSwerving(false);
                breakoutTime = 5;
            }

            yield return new WaitForFixedUpdate();
		}

        if(s != null)
		{
            if(!s.SnailHitTree())
			{
                StartCoroutine(RunAwayFromPlayerFurthest(s));
            }
		}
		else
		{
            StartCoroutine(RunAwayFromPlayerFurthest(s));
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponentInChildren<NavMeshAgent>();

        speed = agent.speed;

        if(flying)
		{
            agent.enabled = false;
		}

        SetMovement(moveOnStart);

        bug.SetMovement(this);

        if(speedMultiplier == 0)
		{
            bug.SetStationaryBug();
		}
		else
		{
            if(flying)
			{
                StartCoroutine(PatrolFlyingPoints());
			}
			else
			{
                StartCoroutine(PatrolPoints());
			}
		}
    }

    public void SetMovement(bool set)
	{
        if (!set)
        {
            StopAllCoroutines();

            if (agent != null)
            {
                agent.speed = 0;
            }
        }
		else
		{
            agent.speed = speed;

            if (!flying)
            {
                isPatrolling = true;
                StartCoroutine(PatrolPoints());
            }
            else
            {
                isPatrolling = true;
                StartCoroutine(PatrolFlyingPoints());
            }
        }
	}

    private void OnDrawGizmos()
    {
        foreach (Transform t in patrolPoints)
        {
            Gizmos.DrawIcon(t.position, "AvatarSelector", false, Color.blue);
        }
    }

    public void SetSpeed(bool faster)
	{
        if(faster)
		{
            agent.speed = speed * speedMultiplier;
		}
		else
		{
            agent.speed = speed;
		}
	}

    public NavMeshAgent GetAgent()
	{
        return agent;
	}
}
