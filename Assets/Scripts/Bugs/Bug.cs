using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bug : MonoBehaviour
{
    [SerializeField]
    protected JournalEntry bugData = default;

    [SerializeField]
    protected bool canCapture = false;

    [SerializeField]
    protected AK.Wwise.Event alertNoise = default;

    [SerializeField]
    protected AK.Wwise.Event searchNoise = default;

    [SerializeField]
    protected AK.Wwise.Event idleNoise = default;

    [SerializeField]
    protected AK.Wwise.Event stopNoises = default;

    public enum BehaviorState { IDLE, SEARCHING, ALERT}

    private BehaviorState currentState = default;
    private BehaviorState previousState = default;


    private GameObject player = default;
    private float distanceToPlayer = 0;

    private bool cooldown = false;

    [SerializeField]
    protected Image emoticon = default;

    private BugMovement movement = default;

    private bool isStationary = false;

	private void Start()
	{
        currentState = BehaviorState.ALERT;
        previousState = currentState;

        ShowEmoticon(false);

        UpdateCurrentState(BehaviorState.IDLE);

        AkSoundEngine.PostEvent(idleNoise.Name, gameObject);
        emoticon.sprite = BugEmoticons.Instance.GetSprite(BugEmoticons.Emoticons.DOCILE);
    }


    private void FixedUpdate()
    {
        if (player != null && !cooldown)
        {
            distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

            if (distanceToPlayer < 3)
            {
                StartCoroutine(movement.RunAwayFromPlayerFurthest(null));
                cooldown = true;
                Invoke("StopCooldown", 2);
            }
        }
    }

    public void SetMovement(BugMovement m)
	{
        movement = m;
	}

    void StopCooldown()
	{
        cooldown = false;
	}

	public virtual void CatchBug()
	{
        BugTypeHandler.Instance.CaughtBug(bugData);
        AkSoundEngine.PostEvent(stopNoises.Name, gameObject);

        movement.StopAllCoroutines();

        Destroy(gameObject);
	}

    public bool GetCanCapture()
    {
        return canCapture;
    }

    public void UpdateCurrentState(BehaviorState s)
	{
        if(s != currentState)
		{
            AkSoundEngine.PostEvent(stopNoises.Name, gameObject);

            currentState = s;

            switch(s)
			{
                case BehaviorState.ALERT:
                    AkSoundEngine.PostEvent(alertNoise.Name, gameObject);
                    emoticon.sprite = BugEmoticons.Instance.GetSprite(BugEmoticons.Emoticons.STARTLED);
                    break;
                case BehaviorState.IDLE:
                    AkSoundEngine.PostEvent(idleNoise.Name, gameObject);
                    emoticon.sprite = BugEmoticons.Instance.GetSprite(BugEmoticons.Emoticons.DOCILE);
                    break;
                case BehaviorState.SEARCHING:
                    AkSoundEngine.PostEvent(searchNoise.Name, gameObject);
                    emoticon.sprite = BugEmoticons.Instance.GetSprite(BugEmoticons.Emoticons.STARTLED);
                    break;
			}

            previousState = currentState;

            ShowEmoticon(true);
            Invoke("HideEmoticon", 5);
        }
	}

    public void ShowEmoticon(bool set)
	{
        if(!set)
		{
            StartCoroutine(FadeEmoticon(0));
		}
		else
		{
            StartCoroutine(FadeEmoticon(1));
        }
	}

    IEnumerator FadeEmoticon(float target)
	{
        float elapsedTime = 0;
        float startValue = emoticon.color.a;
        float duration = 0.5f;

        while (elapsedTime < duration)
		{
            elapsedTime += Time.deltaTime;
            float newAlpha = Mathf.Lerp(startValue, target, elapsedTime / duration);
            emoticon.color = new Color(emoticon.color.r, emoticon.color.g, emoticon.color.b, newAlpha);
            yield return null;
        }

	}

    void HideEmoticon()
	{
        ShowEmoticon(false);
	}

    public void SetStationaryBug()
	{
        isStationary = true;
        emoticon.sprite = BugEmoticons.Instance.GetSprite(BugEmoticons.Emoticons.DOCILE);
    }

	private void OnTriggerEnter(Collider other)
	{
		if(other.transform == Player.Instance.transform)
		{
            player = Player.Instance.gameObject;

            if(isStationary)
			{
                ShowEmoticon(true);
                Invoke("HideEmoticon", 3);
            }
		}
	}

	private void OnTriggerExit(Collider other)
	{
        if (other.transform == Player.Instance.transform)
        {
            player = null;
        }
    }
}
