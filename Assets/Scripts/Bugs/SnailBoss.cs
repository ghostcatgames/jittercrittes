using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SnailBoss : BossBug
{
    IEnumerator RunWithPlayer()
    {
        GetComponent<Bug>().UpdateCurrentState(BehaviorState.ALERT);
        ShowEmoticon(false);

        snailInstructions.SetActive(true);

        isRunning = true;

        movement.SetSpeed(true);

        float time = runAwayTime;

        Player.Instance.StopPlayerInput(true);
        Player.Instance.transform.SetParent(GetBoss().transform);
        Player.Instance.EnableController(false);
        Player.Instance.transform.position = playerSitPoint.position;

        while (isRunning)
		{
            ShowEmoticon(false);

            if (runAwayTime > 0)
            {
                time -= Time.deltaTime;

                if (time <= 0)
                {
                    movement.SetSpeed(false);
                    movement.SetMovement(false);
                    isRunning = false;

                    TossPlayer();
                }
            }
            yield return new WaitForFixedUpdate();
		}

        snailInstructions.SetActive(false);
    }

    IEnumerator CoolDown()
	{
        float time = coolDownTime;

        movement.SetSpeed(true);
        movement.SetMovement(true);

        while (time > 0)
		{
            time -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
		}

        runningWithPlayer = false;
    }

    [SerializeField]
    private float runAwayTime = 0;

    [SerializeField]
    private float coolDownTime = default;

    [SerializeField]
    private Transform playerSitPoint = default;

    private bool isRunning = false;

    private bool runningWithPlayer = false;

    [SerializeField]
    private PlayerInput input = default;

    private bool nearTree = false;
    private GameObject tree = default;

    private bool isSwerving = false;

    [SerializeField]
    private int numberOfHits = 0;

    [SerializeField]
    private GameObject snailInstructions = default;

    // Start is called before the first frame update
    void Start()
    {
        canCapture = false;

        if (input != null)
        {
            input.enabled = false;
        }

        snailInstructions.SetActive(false);

        UpdateCurrentState(BehaviorState.IDLE);

        AkSoundEngine.PostEvent(idleNoise.Name, gameObject);
        emoticon.sprite = BugEmoticons.Instance.GetSprite(BugEmoticons.Emoticons.DOCILE);
    }


    public void PlayerHit()
	{
        if (!runningWithPlayer)
        {
            runningWithPlayer = true;
            input.enabled = true;
            StartCoroutine(RunWithPlayer());
        }
    }

    public void TossPlayer()
	{
        Player.Instance.EnableController(true);

        Player.Instance.ThrowPlayer(-bug.transform.forward);

        Player.Instance.StopPlayerInput(false);
        Player.Instance.transform.SetParent(null);

        input.enabled = false;

        isRunning = false;

        if (numberOfHits > 0)
        {
            StartCoroutine(CoolDown());
        }
    }

	private void OnTriggerEnter(Collider other)
	{
		if(other.transform == Player.Instance.transform)
		{
            PlayerHit();
		}

        if(other.transform.tag == "Tree")
		{
            nearTree = true;
            tree = other.gameObject;
		}
	}

	private void OnTriggerExit(Collider other)
	{
        if (other.transform.tag == "Tree")
        {
            nearTree = false;
            tree = null;
        }
    }

	public void Swerve(InputAction.CallbackContext ctx)
	{
        if(ctx.performed && !isSwerving)
		{
            Vector2 dir = ctx.ReadValue<Vector2>();

            if(nearTree)
			{
                Vector3 direction = bug.transform.InverseTransformPoint(tree.transform.position);

                if (direction.x < 0.0f && dir.x < 0)
                {
                    Debug.Log("LEFT");
                    isSwerving = true;
                    isRunning = false;
                    StopCoroutine(RunWithPlayer());
                    StartCoroutine(movement.MoveTowardsGoal(this, tree.transform.position));
                }
                else if (direction.x > 0.0f && dir.x > 0)
                {
                    Debug.Log("RIGHT");
                    isSwerving = true;
                    isRunning = false;
                    StopCoroutine(RunWithPlayer());
                    StartCoroutine(movement.MoveTowardsGoal(this, tree.transform.position));
                }
				else
				{
                    Debug.Log("Wrong direction hit");
                }          
			}
		}
	}

    public void SetSwerving(bool set)
	{
        isSwerving = set;
	}

    public bool GetIfSwerving()
	{
        return isSwerving;
	}

    public bool SnailHitTree()
	{
        numberOfHits--;

        if(numberOfHits <= 0)
		{
            movement.SetMovement(false);
            canCapture = true;

            TossPlayer();

            Debug.Log("CAN CAPTURE SNAIL BOSS");
            return true;
		}

        return false;
	}
}
