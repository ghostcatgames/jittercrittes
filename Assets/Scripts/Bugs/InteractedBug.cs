using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractedBug : Interactable
{
    [SerializeField]
    private GameObject bug = default;

    // Start is called before the first frame update
    void Start()
    {
        bug.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public override void Interact()
	{
		base.Interact();
        bug.SetActive(true);
	}
}
