using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossArea : MonoBehaviour
{
    [SerializeField]
    private BossBug boss = default;

	[SerializeField]
	private SphereCollider col = default;

	private void OnTriggerEnter(Collider other)
	{
		if(other.transform == Player.Instance.transform)
		{
            boss.SetInArea(true);
		}
	}

	private void OnTriggerExit(Collider other)
    {
        if (other.transform == Player.Instance.transform)
        {
            boss.SetInArea(false);
        }
    }

	public void BossCaught()
	{
		Destroy(gameObject);
		AudioHandler.Instance.SetGameMusicState();
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(col.transform.position, col.radius);
	}
}
