using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationScript : MonoBehaviour
{
    [SerializeField]
    private float speed = 0;

    [SerializeField]
    private Vector3 axis = default;

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.eulerAngles += axis * speed * Time.deltaTime;
    }
}
