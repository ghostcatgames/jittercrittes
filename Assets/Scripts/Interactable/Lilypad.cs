using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lilypad : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{
		if(other.transform == Player.Instance.transform)
		{
            LilypadHandler.Instance.LilypadHit(this);
		}
	}
}
