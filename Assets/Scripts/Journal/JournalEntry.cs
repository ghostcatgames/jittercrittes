using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "Journal Entry", menuName = "New Journal Entry", order = 1)]
public class JournalEntry : ScriptableObject
{
    [SerializeField]
    public string BugName = "";

    [SerializeField]
    public IconData.BugName iconID = default;

    [SerializeField]
    public Sprite BugPicture = default;

    [SerializeField]
    [TextArea(10, 100)]
    public string BugInfo = "";

    [System.Serializable]
    public struct BugInfoUnlocks
	{
        public string info;
        public int bugsCaughtToUnlock;
	}

    [SerializeField]
    public List<BugInfoUnlocks> BugInfoLines = new List<BugInfoUnlocks>();

    [SerializeField]
    public bool SeperateLines = false;

}
