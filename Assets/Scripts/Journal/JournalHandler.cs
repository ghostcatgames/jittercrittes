using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[System.Serializable]
public class IconData
{
    public enum BugName { BEETLE1, BEETLE2, BUTTERFLY1, BUTTERFLY2, BUTTERFLY3, DRAGONFLY1, DRAGONFLY2, FLYCRITTER, LADYBUG1, LADYBUG2, MOTHCRITTER, MOTHBOSS, SNAILBOSS}

    [SerializeField]
    public BugName bugName = default;

    [SerializeField]
    public RenderTexture iconObject = default;

    [SerializeField]
    public GameObject camera = default;

}
public class JournalHandler : MonoBehaviour
{
    public static JournalHandler Instance = default;

    [SerializeField]
    private TextMeshProUGUI bugNameText = default;

    [SerializeField]
    private TextMeshProUGUI bugInfoText = default;

    [SerializeField]
    private RawImage bugSprite = default;

    [SerializeField]
    private Transform galleryParent = default;

    [SerializeField]
    private GameObject galleryButtonPrefab = default;

    [SerializeField]
    List<IconData> bugIcons = new List<IconData>();

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

	private void OnEnable()
	{
        if (BugTypeHandler.Instance.GetBugCaughtCount() > 0)
        {
            FillJournalPage(BugTypeHandler.Instance.GetBugData(0));
            AssignButtonData();

            ShowCorrectBugIcons();
        }
	}

	private void OnDisable()
	{
        HideIcons();
    }

    public void HideIcons()
	{
        foreach (IconData i in bugIcons)
        {
            i.camera.SetActive(false);
        }
    }

	public void FillJournalPage(BugData data)
	{
        bugNameText.text = data.bugJournal.BugName;
        bugSprite.texture = GetBugTex(data.bugJournal.iconID);

        bugInfoText.text = "";

        if (data.bugJournal.SeperateLines)
		{
            foreach(JournalEntry.BugInfoUnlocks s in data.bugJournal.BugInfoLines)
			{
                if (s.bugsCaughtToUnlock <= data.totalCaught)
                {
                    bugInfoText.text += s.info + "\n";
                }
			}
		}
		else
		{
            bugInfoText.text = data.bugJournal.BugInfo;
        }
	}

    public void AssignButtonData()
    {
        int count = BugTypeHandler.Instance.GetBugCaughtCount();

        foreach(Transform t in galleryParent)
		{
            Destroy(t.gameObject);
		}

        for (int i = 0; i < count; i++)
        {
            GameObject newButton = default;

            foreach (IconData b in bugIcons)
			{
                if(b.bugName == BugTypeHandler.Instance.GetBugData(i).bugJournal.iconID)
				{
                    newButton = Instantiate(galleryButtonPrefab);
                    newButton.GetComponent<RawImage>().texture = b.iconObject;
				}
			}
           
            newButton.transform.SetParent(galleryParent);
            newButton.GetComponent<GalleryButton>().AssignData(BugTypeHandler.Instance.GetBugData(i));
        }

    }

    void ShowStandInPage()
	{
        bugNameText.text = "No Bugs Recorded";
        bugSprite.texture = null;

        bugInfoText.text = "";
    }

    public Texture GetBugTex(IconData.BugName b)
	{
        foreach(IconData i in bugIcons)
		{
            if(i.bugName == b)
			{
                return i.iconObject;
			}
		}

        return null;
	}

    public void ShowCorrectBugIcons()
	{
        foreach (IconData i in bugIcons)
        {
            i.camera.SetActive(false);
        }

        foreach (BugData b in BugTypeHandler.Instance.GetBugsCaught())
		{
            foreach (IconData i in bugIcons)
            {
                if (b.bugJournal.iconID == i.bugName)
                {
                    i.camera.SetActive(true);
                }
            }       
		}
	}

    public void ShowSpecificIcon(IconData.BugName b)
	{
        foreach(IconData i in bugIcons)
		{
            if(i.bugName == b)
			{
                i.camera.SetActive(true);
			}
			else
			{
                i.camera.SetActive(false);
            }
		}
	}
}
